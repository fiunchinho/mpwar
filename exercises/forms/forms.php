<?php
//if ( isset( $_POST['name'] ) )
//if ( isset( $_POST['submit'] ) )
if ( $_SERVER['REQUEST_METHOD'] == 'POST' )
{
	$output = "";
	if ( !empty( $_POST['name'] ) )
	{
		$output .= "Your name is: " . $_POST['name'] . ".\n";
	}
	if ( !empty( $_POST['age'] ) )
	{
		$output .= "You are " . $_POST['age'] . " years old.\n";
	}
	if ( !empty( $_POST['gender'] ) )
	{
		$output .= "You are a " . $_POST['gender'] . ".\n";
	}
	if ( !empty( $_POST['email'] ) )
	{
		$output .= "I can contact you in this email address: " . $_POST['email'];
	}
	if ( !empty( $_POST['phone'] ) )
	{
		$output .= " or with this phone number: " . $_POST['phone'] . "\n";
	}
	if ( !empty( $_POST['hobbies'] ) )
	{
		$output .= "Your hobbies are:<ul>";
		foreach( $_POST['hobbies'] as $hobbie )
		{
			$output .= "<li>$hobbie</li>";
		}
	}

	echo $output;
}
else
{
	$form_html =<<<HTML
<form action="forms.php" method="POST">
	<fieldset>
		<label for="name">Name</label>
		<input type="text" id="name" name="name" class="form-text" />
		<p class="form-help">This is help text under the form field.</p>

		<label for="email">Email</label>
		<input type="email" id="email" name="email" class="form-text" required />
		<p class="form-help">Must be a valid email</p>

		<label for="age">Age</label>
		<input type="number" id="age" name="age" class="form-text" required />

		<label for="phone">Phone</label>
		<input type="tel" id="phone" name="phone" class="form-text" />
	</fieldset>

	<fieldset>
		<label for="gender">Gender</label>
		<select id="gender" name="gender">
			<option>Male</option>
			<option>Female</option>
			<option>I'd rather not to say</option>
		</select>

		<label for="hobbies">Hobbies</label>
		<input type="checkbox" name="hobbies[]" value="Reading"> Reading
		<input type="checkbox" name="hobbies[]" value="Football"> Football
		<input type="checkbox" name="hobbies[]" value="Computers"> Computers
		<input type="checkbox" name="hobbies[]" value="Music"> Music
	</fieldset>

	<fieldset>
		<label for="hobbies">Hobbies</label><br />
		<input type="checkbox" name="hobbies[]" value="Reading"> Reading
		<input type="checkbox" name="hobbies[]" value="Football"> Football
		<input type="checkbox" name="hobbies[]" value="Computers"> Computers
		<input type="checkbox" name="hobbies[]" value="Music"> Music
	</fieldset>

	<fieldset class="form-actions">
		<input type="submit" value="Send!" />
	</fieldset>
</form>
HTML;
	echo $form_html;
}
