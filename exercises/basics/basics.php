<?php
// Exercise #1
for( $i = 0; $i < 13; $i++ )
{
	echo $i * $argv[1];
}



// Exercise #2
if ( !isset( $argv[1], $argv[2], $argv[3] ) )
{
	die( 'You need to insert the operation and two numbers.' );
}

if ( $argv[1] == 'add' )
{
	return $argv[2] + $argv[3];
}
else if ( $argv[1] == 'multiply' )
{
	return $argv[2] * $argv[3];
}
else if ( $argv[1] == 'substract' )
{
	return $argv[2] - $argv[3];
}
else if ( $argv[1] == 'divide' )
{
	if ( $argv[3] == 0 )
	{
		die( 'You can\'t divide by 0' );
	}

	return $argv[2] / $argv[3];
}




// Exercise #3
if ( !isset( $argv[1], $argv[2], $argv[3] ) )
{
	die( 'You need to insert the operation and two numbers.' );
}

$result = 0;

if ( $argv[1] == 'add' )
{
	for ( $i=2; $i < $argc; $i++ )
	{
		$result = $result + $argv[$i];
	}
}
else if ( $argv[1] == 'multiply' )
{
	for ( $i=2; $i < $argc; $i++ )
	{
		$result = $result * $argv[$i];
	}
}
else if ( $argv[1] == 'substract' )
{
	for ( $i=2; $i < $argc; $i++ )
	{
		$result = $result - $argv[$i];
	}
}
else if ( $argv[1] == 'divide' )
{
	for ( $i=2; $i < $argc; $i++ )
	{
		if ( $argv[$i] == 0 )
		{
			die( 'You can\'t divide by 0' );
		}
		$result = $result / $argv[$i];
	}
}
echo $result;






// Exercise #4
$greetings = array(
	'Good morning Vietnam!',
	'Whats up?',
	'Hello there!'
);
if ( !isset( $argv[1] ) )
{
	$chosen_greeting = mt_rand( 0, count( $greetings ) );
}
else
{
	$chosen_greeting = $argv[1];
}

echo $greetings[$chosen_greeting];




// Exercise #5
$original_string = 'You play football! Football is one of my favourite sports too. What others sports do you like? Which one do you play?';
$string = strtolower( $original_string );
$string = str_replace( '!', '', $string );
$string = str_replace( '?', '', $string );
$string = str_replace( '.', '', $string );
foreach( explode( ' ', $string ) as $word )
{
	$occurences[$word] = substr_count( $string, $word );
}
echo "Total words:\t" . count( $original_string );
echo "Different words:\t\t" . count( $occurences );
echo "\n";

if ( isset( $argv[2] ) )
{
	sort( $occurences );
}

foreach( $occurences as $word => $number_of_occurences )
{
	echo "$word:\t\t" . $number_of_occurences . "\n";
}
