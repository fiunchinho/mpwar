<?php
// Adding a task
file_put_contents( 'to-do', "\n- " . $argv[1], FILE_APPEND | LOCK_EX );

// Removing by task name
$todo_list = file_get_contents( 'to-do' );
$todo_list = str_replace( $argv[1], '', $todo_list );
file_put_contents( 'to-do', $todo_list );

// Removing by task id
$todo_list = file( 'to-do' );
unset( $todo_list[$argv[1]] );
file_put_contents( 'to-do', $todo_list );


// List of tasks
$todo_list = file( 'to-do' );
echo "To-Do\n============\n";
for ( $i=1; $i < count( $todo_list ); $i++ )
{
	echo "#$i: $task\n";
}



// Removing by task id, inserting in the 'done' file
$todo_list = file( 'to-do' );
$done_task = $todo_list[$argv[1]];
file_put_contents( 'done', "\n- " . $done_task, FILE_APPEND | LOCK_EX );
unset( $todo_list[$argv[1]] );
file_put_contents( 'to-do', $todo_list );



// List of done tasks
$todo_list = file( 'done' );
echo "Done tasks\n============\n";
for ( $i=1; $i < count( $todo_list ); $i++ )
{
	echo "#$i: $task\n";
}