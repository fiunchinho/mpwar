<?php
error_reporting( -1 );
ini_set( "display_errors", "on" );
ini_set( "html_errors", "on" );

session_start();
if ( isset( $_SESSION['username'] ) || isset( $_COOKIE['username'] ) )
{
	header( 'Location: hello.php' );
}

if ( $_SERVER['REQUEST_METHOD'] == 'POST' )
{
	if ( !isset( $_POST['username'], $_POST['password'] ) )
	{
		echo "You need to provide username and password";
	}

	if ( !checkCredentials( $_POST['username'], $_POST['password'] ) )
	{
		echo "Wrong username or password!";
	}
	else
	{
		if ( $_POST['remember'] )
		{
			setcookie( "remember", strtolower( $_POST['username'] ), time()+ 60 * 60 );  /* expire in 1 hour */
		}
		
		$_SESSION['username'] = strtolower( $_POST['username'] );
		header( 'Location: hello.php' );
		die; //exit;
	}
}
else
{
	echo getLoginFormHtml();
}

function getLoginFormHtml()
{
	return <<<HTML
<form action="login.php" method="POST">
	<fieldset>
		<label for="username">Username</label>
		<input type="text" id="username" name="username" required />

		<label for="password">Password</label>
		<input type="password" id="password" name="password" required />
	</fieldset>

	<fieldset>
		<label><input name="remember" type="checkbox" />Remember me</label>
	</fieldset>

	<fieldset class="form-actions">
		<input type="submit" value="Send!" />
	</fieldset>
</form>
HTML;
}

function checkCredentials( $post_username, $post_password )
{
	$credentials = file( "users.txt", FILE_IGNORE_NEW_LINES );

	foreach( $credentials as $user )
	{
		list( $username, $password ) = explode( ',', $user );

		$username 		= strtolower( $username );
		$password 		= strtolower( $password );
		$post_username 	= strtolower( $post_username );
		$post_password 	= strtolower( $post_password );

		if ( $post_username == $username && $post_password == $password )
		{
			return true;
		}
	}

	return false;
}


?>