<?php
error_reporting( -1 );
ini_set( "display_errors", "on" );
ini_set( "html_errors", "on" );
?>

<?php
session_start();

if ( !isset( $_SESSION['username'] ) )
{
	if ( !isset( $_COOKIE['remember'] ) )
	{
		//echo "<h1>Permission denied!</h1>";
		header( 'Location: login.php' );
	}
	else
	{
		$_SESSION['username'] = $_COOKIE['remember'];
	}
}