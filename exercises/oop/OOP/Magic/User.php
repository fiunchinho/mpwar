<?php

namespace OOP\Magic;

class User
{
	private $properties = array();

	public function __get( $index )
	{
		return $this->getPropertyValue( $index );
	}

	public function __set( $index, $value )
	{
		$this->properties[$index] = $value;
	}

	public function __call( $method_name, array $arguments )
	{
		$prefix 	= substr( $method_name, 0, 3 );
		$property 	= strtolower( substr( $method_name, 3 ) );

		switch ( $prefix ) {
			case 'get':
				return $this->getPropertyValue( $index );
				break;

			case 'set':
				$this->properties[$property] = $arguments[0];
				break;

			default:
				throw new \BadMethodCallException( 'The method does not exists' );
				break;
		}
	}

	private function getPropertyValue( $index )
	{
		if ( array_key_exists( $index, $this->properties ) )
		{
			return $this->properties[$index];
		}

		return null;
	}
}