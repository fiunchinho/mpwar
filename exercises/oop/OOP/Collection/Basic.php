<?php

namespace OOP\Collection;

class Basic
{
	protected $items = array();

	public function __construct( $items = array() )
	{
		$this->items = $items;
	}

	public function get( $index )
	{
		if ( array_key_exists( $index, $this->items ) )
		{
			return $this->items[$index];
		}

		return null;
	}

	public function add( $value, $index )
	{
		$this->items[$index] = $value;
	}

	public function count()
	{
		return count( $this->items );
	}

	public function contains( $item_to_search )
	{
		foreach( $this->items as $item )
		{
			if ( $item === $item_to_search )
			{
				return true;
			}
		}

		return false;
	}

	public function sort( $callback = null )
	{
		if ( is_null( $callback ) )
		{
			sort( $this->items );
		}
		else
		{
			usort( $this->items, $callback );
		}
	}

	public function map( $callback )
	{
		array_walk( $this->items, $callback );
		// Equivalent to: $this->items 	= array_map( $callback, $this->items );
	}
}