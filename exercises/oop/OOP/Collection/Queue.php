<?php

namespace OOP\Collection;

class Queue extends \OOP\Collection\Basic
{
	public function get( $index = null )
	{
		return array_shift( $this->items );
	}

	public function add( $value, $index = null )
	{
		$this->items[] = $value;
	}
}