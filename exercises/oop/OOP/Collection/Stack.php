<?php

namespace OOP\Collection;

class Stack extends \OOP\Collection\Basic
{
	public function get( $index = null )
	{
		return array_pop( $this->items );
	}

	public function add( $value, $index = null )
	{
		$this->items[] = $value;
	}
}