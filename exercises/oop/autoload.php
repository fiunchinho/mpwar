<?php

function __autoload( $classname )
{
	// $classname contains something like OOP\Collection\Basic
	// so we need to replace every '\' with '/' to make it a valid path
    include_once str_replace('\\', '/', $classname ) . '.php';
}