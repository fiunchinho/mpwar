<?php

error_reporting( E_ALL | E_STRICT );
ini_set( "display_errors", "on" );
ini_set( "html_errors", "on" );

include_once __DIR__ . '/autoload.php';

$map_callback 	= function( &$item ){ return $item * $item; };
$walk_callback 	= function( &$item, $key ){ $item = $item * $item; };

$collection = new \OOP\Collection\Basic();
$collection->add( 2, 0 );
$collection->add( 3, 1 );
$collection->add( 4, 2 );
$collection->map( $walk_callback );


var_dump( $collection->get(0) );
var_dump( $collection->get(1) );
var_dump( $collection->get(2) );