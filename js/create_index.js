var index 		= document.createElement( 'ul' );
var sections 	= document.getElementById( 'right-col' ).getElementsByTagName( 'h2' );
for (var i = 0; i < sections.length; i++){
	var section 			= document.createElement( 'li' );
	var section_link 		= document.createElement( 'a' );
	section_link.href 		= '#' + sections[i].parentNode.id;
	section_link.innerHTML 	= sections[i].innerHTML;
	section.appendChild( section_link );
	index.appendChild( section );
}
document.getElementsByTagName( 'nav' )[0].appendChild( index );