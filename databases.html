<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<title>MPWAR: PHP</title>

	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Highlight your code with Highlight.js !-->
	<script src="js/highlight.pack.js"></script>
	<link href="css/railscasts.css" media="screen, projection" rel="stylesheet" type="text/css">

	<link href="css/normalize.css" media="screen, projection" rel="stylesheet" type="text/css">
	<link href="css/style.css" media="screen, projection" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.min.css" media="screen, projection" rel="stylesheet" type="text/css">
	
	<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--Fonts from Google"s Web font directory at http://google.com/webfonts -->
	<link href='css/font.css' rel='stylesheet' type='text/css'>
</head>

<body>
	<div id="container">
		<div id="left-col">
			<header>
				<h2>MPWAR: PHP</h2>
				<nav>
				</nav>
			</header>
		</div>
		<div id="right-col">
			<article>
				<h1 id="title">Talking to the database</h1>
				<section id="introduction">
				    <h2>Introduction</h2>
				    <p>
				        A database is a persistent place where you can save information for later retrieval. There are lots of different databases systems out there. You can use almost all of them in your PHP applications, however, in this course we'll use <a title="MySQL" href="http://www.mysql.com/" target="_blank">MySQL</a>.
				    </p>
				    <p>
				    	PHP provides different ways to connect to a <a title="MySQL" href="http://www.mysql.com/" target="_blank">MySQL</a> database. Historically, the standard way to connect was through functions that started by <code>mysql_*</code>, like <code>mysql_connect()</code> or <code>mysql_query()</code>. These functions are marked as deprecated now and, although you'll see many examples on the internet using these functions, <strong>you should avoid them</strong>. They'll be eliminated eventually and they are not secure.
				    </p>
				    <p>
				    	Instead, you should use the <code>mysqli_*</code> (mysql improved) functions or <code>PDO</code> (PHP Data Object). The first one provides both procedural and object oriented interfaces. We'll focus on <code>PDO</code>, a class that lets us talk with the database.
				    </p>
				</section>

				<section id="pdo">
				    <h2>PDO</h2>
				    <p>
				        The PHP Data Objects (PDO) extension defines a lightweight, consistent interface for accessing databases in PHP. <code>PDO</code> provides a data-access abstraction layer, which means that, regardless of which database you're using, you use the same functions to issue queries and fetch data. In case of switching databases, you wouldn't need to change your code.
				    </p>
				    <p>
				    	To use it, you need to select <a title="PDO Drivers" href="http://www.php.net/manual/en/pdo.drivers.php" target="_blank">which driver you'll use</a>, depending on the database of choice. In our case, we'll use the <code>PDO_MYSQL</code> driver.
				    </p>
				</section>

				<section id="connection">
					<h2>Connecting to the database</h2>
					<p>
				    	You need a running MySQL server in order to connect to it. This server could be in the same server as your PHP code, or elsewhere. 
				    </p>
				    <p>
				    	Connections are established by creating instances of the <code>PDO</code> base class. It doesn't matter which driver you want to use; you always use the <code>PDO</code> class name. The constructor accepts parameters for specifying the database source (known as the DSN) and optionally for the username and password (if any). If there are any connection errors, a PDOException object will be thrown.
				    </p>

<pre><code class="php">&lt;?php
$db = new PDO('mysql:host=localhost;dbname=test', $user, $pass);
</code></pre>
					<p>
						Upon successful connection to the database, an instance of the <code>PDO</code> class is returned to your script. The connection remains active for the lifetime of that <code>PDO</code> object. To close the connection, you need to destroy the object. If you don't do this explicitly, PHP will automatically close the connection when your script ends.
					</p>
				</section>

				<section id="queries">
					<h2>Querying</h2>
					<p>
						We have different ways for retrieving data from the database. The simplest way is just to call the <code>query()</code> method and iterate over the results.
					</p>
<pre><code class="php">&lt;?php
$rows = $db->query('SELECT * FROM table');
foreach( $rows as $row ) {
    echo $row['field1'].' '.$row['field2'];
}</code></pre>
					<p>
						The <code>query()</code> method returns a <code>PDOStatement</code> object that you can iterate. <a title="PDOStatement" href="http://php.net/manual/en/class.pdostatement.php" target="_blank">This object has other methods</a> to work with the result set.
					</p>
<pre><code class="php">&lt;?php
$statement = $db->query('SELECT * FROM table');
// You can fetch every row one at a time
while( $row = $statement->fetch( PDO::FETCH_ASSOC ) ) {
    echo $row['field1'].' '.$row['field2']; //etc...
}

// Or fetching all the items at once
$results = $statement->fetchAll( PDO::FETCH_ASSOC );
</code></pre>
					<h3>Fetch Modes</h3>
					<p>
						You <a title="Fetch modes" href="http://php.net/manual/en/pdostatement.fetch.php">can configure the way</a> <code>PDO</code> will fetch rows from the database. Note the use of <code>PDO::FETCH_ASSOC</code> in the <code>fetch()</code> and <code>fetchAll()</code> code above. This tells <code>PDO</code> to return the rows as an associative array with the field names as keys. Other fetch modes like <code>PDO::FETCH_NUM</code> returns the row as a numerical array. The default is to fetch with <code>PDO::FETCH_BOTH</code> which duplicates the data with both numerical and associative keys. It's recommended you specify one or the other so you don't have arrays that are double the size.
					</p>
					<p>
						Instead of setting the fetch mode in every query, you could also define a default mode for all queries using <a title="PDO attributes" href="http://php.net/manual/en/pdo.setattribute.php" target="_blank">PDO attributes</a>
					</p>
<pre><code class="php">&lt;?php
$db = new PDO('mysql:host=localhost;dbname=test', $user, $pass );
$db->setAttribute( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );

$statement 	= $db->query('SELECT * FROM table');
$results 	= $statement->fetchAll(); // It'll use FETCH_ASSOC as fetch mode
</code></pre>
				</section>

				<section id="statements">
					<h2>Prepared Statements</h2>
					<p>
						Using parameters in your queries and forget to protect yourself is one the most commons security holes in the internet. If the user can send parameters to your queries, a malicious user could try to get benefit from this using an attack called <a title="SQL Injection" href="http://php.net/manual/en/security.database.sql-injection.php" target="_blank">SQL Injection</a>.
					</p>
					<p>
						To protect yourself against <a title="SQL Injections" href="http://php.net/manual/en/security.database.sql-injection.php" target="_blank">SQL Injections</a> you have to never trust user input and sanitize it. There are PHP functions that could help you with this, but <code>PDO</code> comes with the best tool for the job: prepared statements.
					</p>
					<p>
						Prepared statements are a feature from many database systems that allow you to save queries that you'll run multiple times in the database itself, so you can save execution time: the query is only parsed once so it'll run faster.
					</p>
					<p>
						If you need to execute the same query several times, but with different parameters, that's not a problem. You can bind parameters to this prepared statements. It'll still parse the query only once, but using different parameters each time.
					</p>
					<p>
						When you bind parameters, <code>PDO</code> will automatically sanitize the input so you don't have to worry about <a title="SQL Injections" href="http://php.net/manual/en/security.database.sql-injection.php" target="_blank">SQL Injections</a>.
					</p>
<pre><code class="php">&lt;?php
$email 		= 'john@doe.net';
$password 	= 'mypassword';

# You can send parameters like this:
$stmt 		= $db->prepare( "SELECT * FROM users WHERE email=? AND pass=? ");
$stmt->bindParam( 1, $email, PDO::PARAM_STR );
$stmt->bindParam( 2, $password, PDO::PARAM_STR );
$stmt->execute();
$rows 		= $stmt->fetchAll();

# ... or like this
$stmt 		= $db->prepare( "SELECT * FROM users WHERE id=:email AND pass=:pass ");
$stmt->bindParam( ':email', $email, PDO::PARAM_STR );
$stmt->bindParam( ':pass', $password, PDO::PARAM_STR );
$stmt->execute();
$rows 		= $stmt->fetchAll();

# ... or even like this, althought without type validation
$stmt 		= $db->prepare( "SELECT * FROM users WHERE email=? AND pass=? ");
$stmt->execute( array( $email, $password ) );
$rows 		= $stmt->fetchAll();

# ... equivalent to the last one
$stmt 		= $db->prepare( "SELECT * FROM users WHERE id=:email AND pass=:pass ");
$stmt->execute( array( ':email' => $email, ':password' => $password ) );
$rows 		= $stmt->fetchAll();
</code></pre>
					<p>
						The prepare method sends the query to the server, and it's compiled with the placeholders to be used as expected arguments. The execute method sends the arguments to the server and runs the compiled statement. Since the query and the dynamic parameters are sent separately, there is no way that any SQL that is in those parameters can be executed, so no SQL Injection can occur.
					</p>
					<p>
						PDO also checks for the type of the value. If it's not from the expected type, it'll raise an error.
					</p>
					<p>
						When binding parameters for a prepared statement using named placeholders, this will be a parameter name of the form <em>:name</em>. For a prepared statement using question mark placeholders, <strong>this will be the 1-indexed position of the parameter</strong>.
					</p>
				</section>

				<section id="inserting">
					<h2>Inserting</h2>
					<p>
						All the theory that we've been for querying data from the database, apply to inserting, updating and deleting from it. Same rules.
					</p>
				</section>

				<section id="filtering">
					<h2>Filtering Data</h2>
					<p>
						We've mentioned that you shoulnd't trust user input, because a malicious user may try to break into your system. To avoid this, you should filter the data. PDO does this automatically when you bind params, but we have other methods. PHP has two functions for filtering: <a title="filter_var" href="http://php.net/manual/en/function.filter-var.php" target="_blank">filter_var</a>, for filtering variables; and <a title="filter_input" href="http://php.net/manual/en/function.filter-input.php" target="_blank">filter_input</a>, for filtering super globals. Both functions return the filtered value, or FALSE if the filter fails.
					</p>
<pre><code class="php">&lt;?php
# Filtering variables
$email_a = 'joe@example.com';
$email_b = 'bogus';

var_dump( filter_var($email_a, FILTER_VALIDATE_EMAIL) ); // Prints joe@example.com
var_dump( filter_var($email_b, FILTER_VALIDATE_EMAIL) ); // Prints false

$ip_a = '127.0.0.1';
$ip_b = '42.42';

var_dump( filter_var($ip_a, FILTER_VALIDATE_IP) ); // Prints '127.0.0.1'
var_dump( filter_var($ip_b, FILTER_VALIDATE_IP) ); // Prints false

# Filtering super globals, when $_GET['search'] = Me &#38; son
$search_html 	= filter_input(INPUT_GET, 'search', FILTER_SANITIZE_SPECIAL_CHARS);
$search_url 	= filter_input(INPUT_GET, 'search', FILTER_SANITIZE_ENCODED);
echo "You have searched for $search_html.\n"; // Me &#38; son.
echo "&lt;a href='?search=$search_url'&gt;Search again.&lt;/a&gt;"; // ?search=Me%20%26%20son
</code></pre>
					<p>
						There are two main types of filtering: validation and sanitization.
					</p>
					<h3>Validation</h3>
					<p>
						Validation is used to validate or check if the data meets certain qualifications. For example, passing in FILTER_VALIDATE_EMAIL will determine if the data is a valid email address, but will not change the data itself.
					</p>
<pre><code class="php">&lt;?php
# Filtering variables
$email_a = 'joe@example.com';
$email_b = 'bogus';

var_dump( filter_var($email_a, FILTER_VALIDATE_EMAIL) ); // Prints joe@example.com
var_dump( filter_var($email_b, FILTER_VALIDATE_EMAIL) ); // Prints false
</code></pre>
					<h3>Sanitization</h3>
					<p>
						Sanitization will sanitize the data, so it may alter it by removing undesired characters. For example, passing in FILTER_SANITIZE_EMAIL will remove characters that are inappropriate for an email address to contain. That said, it does not validate the data.
					</p>
<pre><code class="php">&lt;?php
$b = 'bogus - at - example dot org';
$c = '(bogus@example.org)';

$sanitized_b = filter_var($b, FILTER_SANITIZE_EMAIL); // bogus-at-exampledotorg
if (filter_var($sanitized_b, FILTER_VALIDATE_EMAIL)) {
    echo "This sanitized email address is considered valid.";
} else {
    echo "This (b) sanitized email address is considered invalid.\n";
}

$sanitized_c = filter_var($c, FILTER_SANITIZE_EMAIL); // bogus@example.org
if (filter_var($sanitized_c, FILTER_VALIDATE_EMAIL)) {
    echo "This (c) sanitized email address is considered valid.\n";
    echo "Before: $c\n";
    echo "After:  $sanitized_c\n";
}
</code></pre>
				</section>

				<section id="reading">
					<h2>Further Reading</h2>
					<ul>
						<li><a title="MySQLi" href="http://www.php.net/manual/es/book.mysqli.php" target="_blank">MySQLi</a></li>
						<li><a title="PDO" href="http://php.net/manual/es/book.pdo.php" target="_blank">PDO</a></li>
						<li><a title="SQL Injection" href="http://php.net/manual/en/security.database.sql-injection.php" target="_blank">SQL Injection</a></li>
						<li><a title="Why you Should be using PHP’s PDO for Database Access" href="http://net.tutsplus.com/tutorials/php/why-you-should-be-using-phps-pdo-for-database-access/" target="_blank">PDO Tutorial</a></li>
					</ul>
				</section>

				<section id="homework">
			        <h2>Exercises</h2>
			        <h3>Exercise #1</h3>
			        <p>
			        	Change your TODO list exercise, so it uses a MySQL database instead of the filesystem. The table for the todos will be called tasks, and it'll contain the following fields: id, task, date, done. The done field is to indicate if the task has been done or not.
			        </p>
			        <h3>Exercise #2</h3>
			        <p>
				    	We need a class to take care the encryption of the passwords in our application. We'll encrypt our passwords using the Caesar algorythm, that encrypts strings moving each letter 3 positions in the alphabet. The letter 'a' will become the letter 'd'; the letter 'b' will become the letter 'e'; and the letter 'z' will become the letter 'c'.<br />Create a class with two methods <code>encrypt</code> and <code>decrypt</code> that implement the Caesar algorythm.
				    </p>
<pre><code class="php">$encrypter = new Encrypter();
echo $encrypter->encrypt( 'abc' ); // outputs 'def'
echo $encrypter->encrypt( 'yza' ); // outputs 'bcd'
echo $encrypter->decrypt( 'def' ); // outputs 'abc'
echo $encrypter->decrypt( 'bcd' ); // outputs 'yza'
</code></pre>
				    <h3>Registering users</h3>
					<p>
				    	Create a sign up form that users will use to sign up in our website. This form will have 2 fields: username and password. When the user submits the form, his credentials will be saved in a database. The password must be saved encrypted, using the algorythm described above.
				    </p>
				    <h3>Login</h3>
				    <p>
				    	Create a login page that users can use to identify themselves. Only users that have been registered before can log in to the site. The login page contains a form asking for username and password.
				    </p>
				    <p>
				    	Use your encrypter class to see if the submitted credentials are valid. If they are not, show an error message. If they are correct, start a session and redirect the user to a <em>hello world</em> page that can only be seen by authenticated users.
				    </p>
				    <p>
				    	The <em>hello world</em> page is just a page showing Hello <code>$username</code>. If a non authenticated users tries to see this page, it'll be redirected to the login page.
				    </p>
				    <h3>Logout</h3>
				    <p>
				    	Create a logout page that apart from logging the user out, removes the cookie.
				    </p>

					<h3>Notes</h3>
					<ul>
						<li>You can assume that the passwords will contain only characters from 'a' to 'z'.</li>
						<li>Our encrypter is case insensitive, so 'hola' will produce the same password that 'HoLa'.</li>
						<li>Use Namespaces.</li>
						<li>Use autoloading to load files.</li>
					</ul>
			    </section>

			    <section>
			    	<h2>Extra ball</h2>
			    	<p>
			    		Update the extra ball from the <a title="Object Oriented PHP" href="oop.html#extra">previous lesson</a>, where you created a class that represented the <code>$_GET</code> super global. Change it so you can apply filters directly to values like
			    	</p>
<pre><code class="php">&lt;?php
$get = new Get();
$get->getEmail( 'email' ); // Validates that the email parameter is, in fact, a valid email
$get->getString( 'name' ); // Validates that name is a valid string
$get->getNumber( 'age' ); // Validates that age is a valid number
</code></pre>
					<p>
						Create a class for every super global <code>$_GET</code>, <code>$_POST</code>, <code>$_SERVER</code>, so you don't have to use them directly ever again.
					</p>
			    </section>
			</article>
			<footer>
				Previous lesson: <a title="Object Oriented PHP" href="oop.html">Object Oriented PHP</a> | Next lesson: <br /> <a href="index.html">Schedule</a>
			</footer>
		</div>
	</div>
	<script>hljs.initHighlightingOnLoad();</script>
	<script src="js/create_index.js"></script>
</body>
</html>